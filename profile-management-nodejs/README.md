## INSTRUCTIONS TO RUN CODE ##

* Clone the project.
* Goto the cloned project directory and run `npm install`. This will install all dependencies.
* Goto the `config` directory and check each `.json` file and edit values as mentioned.
* Run `export NODE_ENV=dev or test or qa or pre-prod` based on the stage you want to run.
* Run `node server.js`.
* Application should run by using the stage specific `db.Connection` if it's valid.
