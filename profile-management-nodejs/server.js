var Sequelize = require('sequelize')
var config = require('config');//import the config module.

// read db.connection value from the specified profile file.
var dbConfig = config.get('db.Connection');

//print the configuration directory
console.log('NODE_CONFIG_DIR: ' + config.util.getEnv('NODE_CONFIG_DIR'));

//print the NODE_ENV
console.log('NODE_ENV: ' + config.util.getEnv('NODE_ENV'));

//print the dbconfig
console.log("DB IS:-",dbConfig)

//sequelize code to connect to the gievn DB URL.
const Conn = new Sequelize(dbConfig, {
    define: {
      timestamps: false
    }
  }); 