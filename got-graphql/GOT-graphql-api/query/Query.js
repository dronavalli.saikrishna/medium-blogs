var {
    GraphQLObjectType,
    GraphQLList,
    GraphQLSchema,
    GraphQLNonNull,
    GraphQLString,
    GraphQLInt,
    GraphQLBoolean,
    GraphQLInputObjectType,
    GraphQLFloat
    }= require('graphql');
var Db = require('../db');

var Query=new GraphQLObjectType({
    name:"Query",
    description:"Query description",
    fields:()=>({
        age:{
            type:new GraphQLList(require("../types/age/Age")),
            args:{
                offset:{
                    type:GraphQLInt
                },
                limit:{
                    type:GraphQLInt
                },
                FindAgeWith:{
                    type:require("../types/age/AgeInput")
                }
            },
            resolve:(_,args)=>{
                return Db.models.age.findAll({
                    where:args.FindAgeWith,
                    limit:args.limit,
                    offset:args.offset
                })
            }
        },
        characters:{
            type:new GraphQLList(require("../types/characters/Characters")),
            args:{
                offset:{
                    type:GraphQLInt
                },
                limit:{
                    type:GraphQLInt
                },
                FindCharactersWith:{
                    type:require("../types/characters/CharactersInputType")
                }
            },
            resolve:(_,args)=>{
                return Db.models.characters.findAll({
                    where:args.FindCharactersWith,
                    limit:args.limit,
                    offset:args.offset
                })
            }
        },
        city:{
            type:new GraphQLList(require("../types/city/City")),
            args:{
                offset:{
                    type:GraphQLInt
                },
                limit:{
                    type:GraphQLInt
                },
                FindCityWith:{
                    type:require("../types/city/CityInput")
                }
            },
            resolve:(_,args)=>{
                return Db.models.city.findAll({
                    where:args.FindCityWith,
                    limit:args.limit,
                    offset:args.offset
                })
            }
        },
        continents:{
            type:new GraphQLList(require("../types/continents/Continent")),
            args:{
                offset:{
                    type:GraphQLInt
                },
                limit:{
                    type:GraphQLInt
                },
                FindContitnentWith:{
                    type:require("../types/continents/ContinentsInputType")
                }
            },
            resolve:(_,args)=>{
                return Db.models.continents.findAll({
                    where:args.FindContitnentWith,
                    limit:args.limit,
                    offset:args.offset
                })
            }
        },
        culture:{
            type:new GraphQLList(require("../types/cultiure/Culture")),
            args:{
                offset:{
                    type:GraphQLInt
                },
                limit:{
                    type:GraphQLInt
                },
                FindCultureWith:{
                    type:require("../types/cultiure/CultureInputType")
                }
            },
            resolve:(_,args)=>{
                return Db.models.culture.findAll({
                    where:args.FindCultureWith,
                    limit:args.limit,
                    offset:args.offset
                })
            }
        },
        episode:{
            type:new GraphQLList(require("../types/episode/Episode")),
            args:{
                offset:{
                    type:GraphQLInt
                },
                limit:{
                    type:GraphQLInt
                },
                FindEpisodeWith:{
                    type:require("../types/episode/EpisodeInputType")
                }
            },
            resolve:(_,args)=>{
                return Db.models.episode.findAll({
                    where:args.FindEpisodeWith,
                    limit:args.limit,
                    offset:args.offset
                })
            }
        },
        episode_characters:{
            type:new GraphQLList(require("../types/episode-characters/EpisodeCharacters")),
            args:{
                offset:{
                    type:GraphQLInt
                },
                limit:{
                    type:GraphQLInt
                },
                FindEpisodeCharactersWith:{
                    type:require("../types/episode-characters/EpisodeCharactersInput")
                }
            },
            resolve:(_,args)=>{
                return Db.models.episode_characters.findAll({
                    where:args.FindEpisodeCharactersWith,
                    limit:args.limit,
                    offset:args.offset
                })
            }
        },
        event:{
            type:new GraphQLList(require("../types/event/Event")),
            args:{
                offset:{
                    type:GraphQLInt
                },
                limit:{
                    type:GraphQLInt
                },
                FindEventWith:{
                    type:require("../types/event/EventInput")
                }
            },
            resolve:(_,args)=>{
                return Db.models.event.findAll({
                    where:args.FindEventWith,
                    limit:args.limit,
                    offset:args.offset
                })
            }
        },
        house:{
            type:new GraphQLList(require("../types/house/House")),
            args:{
                offset:{
                    type:GraphQLInt
                },
                limit:{
                    type:GraphQLInt
                },
                FindHouseWith:{
                    type:require("../types/house/HouseInput")
                }
            },
            resolve:(_,args)=>{
                return Db.models.house.findAll({
                    where:args.FindHouseWith,
                    limit:args.limit,
                    offset:args.offset
                })
            }
        },
        region:{
            type:new GraphQLList(require("../types/region/Region")),
            args:{
                offset:{
                    type:GraphQLInt
                },
                limit:{
                    type:GraphQLInt
                },
                FindRegionWith:{
                    type:require("../types/region/RegionInput")
                }
            },
            resolve:(_,args)=>{
                return Db.models.region.findAll({
                    where:args.FindRegionWith,
                    limit:args.limit,
                    offset:args.offset
                })
            }
        },
    })
});

module.exports=Query;
