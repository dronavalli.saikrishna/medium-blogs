var { GraphQLNonNull, GraphQLString, GraphQLObjectType } = require('graphql');
var Db = require("../db");
var pubsub = require("../pubsub/pubsub-engine");
var SUBSCRIPTION_EVENTS = require("../pubsub/subscription-events");

var Query = new GraphQLObjectType({
    name: "Mutation",
    description: "Mutations description",
    fields: () => ({
        diableHouse: {
            type: require("../types/house/House"),
            args: {
                houseName: {
                    type: new GraphQLNonNull(GraphQLString)
                }
            },
            resolve: async (_, args, context, info) => {
                console.log(args);
                const house = await Db.models.house.findOne({
                    where: {
                        name: args.houseName,
                    }
                });
                if (house && house.is_extinct === false) {
                    const update = await Db.models.house.update({ is_extinct: true }, {
                        where: {
                            name: args.houseName
                        }
                    })
                    const disabledHouse = await Db.models.house.findOne({
                        where: {
                            name: args.houseName
                        }
                    })
                    pubsub.publish(SUBSCRIPTION_EVENTS.HOUSE_DISABLED, { house: disabledHouse });
                    return disabledHouse;
                }
                else if (house && house.is_extinct === true) {
                    throw new Error("House " + args.houseName + " is already disabled");
                }
                else {
                    throw new Error("House " + args.houseName + " not available.");
                }
            }
        },
        createEpisode: {
            type: require("../types/episode/Episode"),
            args: {
                episode: {
                    type: new GraphQLNonNull(require("../types/episode/EpisodeInputType"))
                }
            },
            resolve: async (_, args, context, info) => {
                console.log(args);
                const createdEpisode = await Db.models.episode.create(args.episode);
                pubsub.publish(SUBSCRIPTION_EVENTS.EPISODE_ADDED, { episode: createdEpisode });
                return createdEpisode;
            }
        }
    })
});

module.exports = Query;