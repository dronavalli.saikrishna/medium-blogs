/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('house', {
		name: {
			type: DataTypes.STRING(500),
			allowNull: false,
			primaryKey: true
		},
		is_extinct: {
			type: DataTypes.BOOLEAN,
			allowNull: true,
			defaultValue: '0'
		},
		coat_of_arms: {
			type: DataTypes.STRING(1000),
			allowNull: true
		},
		words: {
			type: DataTypes.STRING(100),
			allowNull: true
		},
		overlord: {
			type: DataTypes.STRING(500),
			allowNull: true,
			references: {
				model: 'house',
				key: 'name'
			}
		},
		title: {
			type: DataTypes.STRING(100),
			allowNull: true
		},
		region: {
			type: DataTypes.STRING(100),
			allowNull: true,
			references: {
				model: 'region',
				key: 'name'
			}
		},
		cadet_branch: {
			type: DataTypes.STRING(100),
			allowNull: true
		},
		founded: {
			type: DataTypes.STRING(100),
			allowNull: true
		},
		created_at: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
		},
		modified_at: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
		},
		image_link: {
			type: DataTypes.STRING(100),
			allowNull: true
		}
	}, {
		tableName: 'house'
	});
};
