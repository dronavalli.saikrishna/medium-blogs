/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('characters', {
		name: {
			type: DataTypes.STRING(1000),
			allowNull: false,
			primaryKey: true
		},
		male: {
			type: DataTypes.BOOLEAN,
			allowNull: true,
			defaultValue: '0'
		},
		culture: {
			type: DataTypes.STRING(100),
			allowNull: true,
			references: {
				model: 'culture',
				key: 'culture_name'
			}
		},
		age: {
			type: DataTypes.STRING(1000),
			allowNull: true,
			references: {
				model: 'age',
				key: 'name'
			}
		},
		actor: {
			type: DataTypes.STRING(1000),
			allowNull: true
		},
		mother: {
			type: DataTypes.STRING(1000),
			allowNull: true,
			references: {
				model: 'characters',
				key: 'name'
			}
		},
		father: {
			type: DataTypes.STRING(1000),
			allowNull: true,
			references: {
				model: 'characters',
				key: 'name'
			}
		},
		heir: {
			type: DataTypes.STRING(1000),
			allowNull: true,
			references: {
				model: 'characters',
				key: 'name'
			}
		},
		house: {
			type: DataTypes.STRING(1000),
			allowNull: true,
			references: {
				model: 'house',
				key: 'name'
			}
		},
		created_at: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
		},
		modified_at: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
		},
		spouse: {
			type: DataTypes.STRING(1000),
			allowNull: true,
			references: {
				model: 'characters',
				key: 'name'
			}
		},
		allegiance: {
			type: DataTypes.STRING(1000),
			allowNull: true,
			references: {
				model: 'characters',
				key: 'name'
			}
		},
		imageLink: {
			type: DataTypes.STRING(1000),
			allowNull: true
		},
		slug: {
			type: DataTypes.STRING(1000),
			allowNull: true
		}
	}, {
		tableName: 'characters'
	});
};
