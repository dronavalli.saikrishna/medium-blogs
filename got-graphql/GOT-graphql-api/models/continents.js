/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('continents', {
		name: {
			type: DataTypes.STRING(100),
			allowNull: false,
			primaryKey: true
		},
		cardinal_direction: {
			type: DataTypes.STRING(100),
			allowNull: true
		},
		created_time: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
		},
		modified_time: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
		}
	}, {
		tableName: 'continents'
	});
};
