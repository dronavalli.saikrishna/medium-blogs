/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('age', {
		name: {
			type: DataTypes.STRING(500),
			allowNull: false,
			primaryKey: true
		},
		predecessor: {
			type: DataTypes.STRING(100),
			allowNull: true,
			references: {
				model: 'age',
				key: 'name'
			}
		},
		successor: {
			type: DataTypes.STRING(100),
			allowNull: true,
			references: {
				model: 'age',
				key: 'name'
			}
		}
	}, {
		tableName: 'age'
	});
};
