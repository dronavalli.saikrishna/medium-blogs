/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('episode', {
        name: {
            type: DataTypes.STRING(1000),
            allowNull: false,
            primaryKey: true
        },
        season: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        episode_number: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        previous_episode: {
            type: DataTypes.STRING(1000),
            allowNull: true,
            references: {
                model: 'episode',
                key: 'name'
            }
        },
        next_episode: {
            type: DataTypes.STRING(1000),
            allowNull: true,
            references: {
                model: 'episode',
                key: 'name'
            }
        },
        director: {
            type: DataTypes.STRING(1000),
            allowNull: true
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        modified_at: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        }
    }, {
            tableName: 'episode'
        });
};
