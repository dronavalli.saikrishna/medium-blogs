/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('region', {
		name: {
			type: DataTypes.STRING(100),
			allowNull: false,
			primaryKey: true
		},
		color: {
			type: DataTypes.STRING(1000),
			allowNull: true
		}
	}, {
		tableName: 'region'
	});
};
