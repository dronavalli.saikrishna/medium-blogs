/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('event', {
		name: {
			type: DataTypes.STRING(1000),
			allowNull: false,
			primaryKey: true
		},
		date: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		age: {
			type: DataTypes.STRING(100),
			allowNull: true,
			references: {
				model: 'age',
				key: 'name'
			}
		},
		created_at: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
		},
		modified_time: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
		}
	}, {
		tableName: 'event'
	});
};
