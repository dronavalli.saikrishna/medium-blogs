/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('episode_characters', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		episode: {
			type: DataTypes.STRING(1000),
			allowNull: true,
			references: {
				model: 'episode',
				key: 'name'
			}
		},
		characters: {
			type: DataTypes.STRING(1000),
			allowNull: true,
			references: {
				model: 'characters',
				key: 'name'
			}
		}
	}, {
		tableName: 'episode_characters'
	});
};
