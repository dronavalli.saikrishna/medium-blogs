/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('city', {
		name: {
			type: DataTypes.STRING(100),
			allowNull: false,
			primaryKey: true
		},
		x_coordinate: {
			type: DataTypes.STRING(100),
			allowNull: true
		},
		y_coordinate: {
			type: DataTypes.STRING(100),
			allowNull: true
		},
		type: {
			type: DataTypes.STRING(100),
			allowNull: true
		},
		priority: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		},
		link: {
			type: DataTypes.STRING(100),
			allowNull: true
		}
	}, {
		tableName: 'city'
	});
};
