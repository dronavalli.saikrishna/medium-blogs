var Sequelize=require('sequelize');
var config = require('config');

var dbConfig = config.get('db.Connection');

console.log("DB IS:-",dbConfig)

const Conn = new Sequelize(dbConfig, {
    define: {
      timestamps: false
    }
  });

var age=Conn.import(__dirname+"/models/age");
var characters=Conn.import(__dirname+"/models/characters");
var city=Conn.import(__dirname+"/models/city");
var continents=Conn.import(__dirname+"/models/continents");
var culture=Conn.import(__dirname+"/models/culture");
var episode_characters=Conn.import(__dirname+"/models/episode_characters");
var episode=Conn.import(__dirname+"/models/episode");
var event=Conn.import(__dirname+"/models/event");
var house=Conn.import(__dirname+"/models/house");
var region=Conn.import(__dirname+"/models/region");

episode_characters.belongsTo(characters,{foreignKey:'characters', as:"EpisodeCharacters"})
characters.hasMany(episode_characters,{foreignKey:'characters', as:"EpisodeCharacters"})

episode.hasMany(episode_characters,{foreignKey:'episode',as:"Episodes"})
episode_characters.belongsTo(episode,{foreignKey:'episode',as:"Episodes"})


module.exports=Conn;
