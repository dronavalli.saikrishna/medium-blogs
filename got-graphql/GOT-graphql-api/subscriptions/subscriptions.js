var { GraphQLObjectType } = require("graphql");
var { GraphQLObjectType, GraphQLList } = require('graphql');
var SUBSCRIPTION_EVENTS = require("../pubsub/subscription-events");
var Db = require("../db");
var pubsub = require("../pubsub/pubsub-engine");

var Subscription = new GraphQLObjectType({
    name: "Subscription",
    description: "subscriptions description",
    fields: () => ({
        episodeRefresh: {
            type: new GraphQLList(require("../types/episode/Episode")),
            subscribe: () => pubsub.asyncIterator(SUBSCRIPTION_EVENTS.EPISODE_ADDED),
            resolve: (body, args, context, info) => {
                console.log("body is:-", body);
                //event body will be availble under body object.
                /*
                * Please ad your notification logic here. I am returning all episodes based on latest season
                */
                return Db.models.episode.findAll({
                    order: [
                        ['season', 'DESC']
                    ]
                });
            }
        },
        houseDisabled: {
            type: require("../types/house/House"),
            subscribe: () => pubsub.asyncIterator(SUBSCRIPTION_EVENTS.HOUSE_DISABLED),
            resolve: async (body, args, context, info) => {
                // get all allies and inform them that the house is diabled i.e became extinct. Here i am simply resturning the event body.
                console.log(body);
                return body.house;
            }
        }
    })
});

module.exports = Subscription;