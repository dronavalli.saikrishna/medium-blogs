var Express = require('express')
var bodyParser = require('body-parser');
var { graphqlExpress, graphiqlExpress } = require('apollo-server-express');
var { SubscriptionServer } = require("subscriptions-transport-ws");
var { execute, subscribe } = require("graphql");
var cors = require("cors");
var { createServer } = require("http");
var schema = require("./schema");
var Db = require('./db')

const PORT = 4000;

const app = Express();
var corsOptions = {
    optionsSuccessStatus: 200
}
app.use(cors(corsOptions));

app.all('/got/graphql',
    bodyParser.json(),
    graphqlExpress(request => ({
        schema: schema,
        context: { request },
    }))
);
app.get('/got/graphiql', graphiqlExpress(
    {
        endpointURL: '/got/graphql',
        subscriptionsEndpoint: `ws://localhost:${PORT}/subscriptions`
    }
));

const ws = createServer(app);
ws.listen(PORT, () => {
    console.log(`Go to http://localhost:${PORT}/got/graphiql to run queries!`);

    // eslint-disable-next-line
    const subscriptionServer = new SubscriptionServer({
        execute,
        subscribe,
        schema
    }, {
            server: ws,
            path: "/subscriptions"
        });
});

