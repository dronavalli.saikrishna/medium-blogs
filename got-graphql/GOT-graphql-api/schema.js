var Query = require("./query/Query")
var Mutation = require("./mutations/mutation");
var Subscription = require("./subscriptions/subscriptions");
var { GraphQLSchema, GraphQLIncludeDirective, GraphQLSkipDirective } = require('graphql')


var schema = new GraphQLSchema({
    query: Query,
    mutation: Mutation,
    subscription: Subscription
});

module.exports = schema;
