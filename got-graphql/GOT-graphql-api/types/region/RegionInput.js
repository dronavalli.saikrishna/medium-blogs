var {GraphQLInputObjectType, GraphQLString,GraphQLBoolean} = require('graphql')

var RegionInput= new GraphQLInputObjectType ({
    name:"RegionInput",
    description:"Type for region input",
    fields:()=>({
        name: {
			type: GraphQLString
		},
		color: {
			type: GraphQLString
        }
    })
})

module.exports=RegionInput;