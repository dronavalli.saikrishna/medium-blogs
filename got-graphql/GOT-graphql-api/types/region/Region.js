var {GraphQLObjectType, GraphQLString,GraphQLBoolean,GraphQLList} = require('graphql')
var Db= require("../../db")

var Region= new GraphQLObjectType ({
    name:"Region",
    description:"Type for region output",
    fields:()=>({
        name: {
			type: GraphQLString
		},
		color: {
			type: GraphQLString
        },
        HousesInTheRegion:{
            type: new GraphQLList(require("../house/House")),
             resolve:(region,args)=>{
                 return Db.models.house.findAll({
                     where:{
                         region:region.name
                     }
                 })
             }
        }
    })
})

module.exports=Region;