var {GraphQLObjectType, GraphQLString,GraphQLBoolean} = require('graphql')

var ContinentsType= new GraphQLObjectType({
    name:"ContinentsType",
    description:"Type for continents output",
    fields:()=>({   
        name: {
			type: GraphQLString
		},
		cardinal_direction: {
			type: GraphQLString
		},
		created_time: {
			type: GraphQLString
		},
		modified_time: {
			type: GraphQLString
		}
    })
})

module.exports=ContinentsType;