var {GraphQLInputObjectType, GraphQLString,GraphQLBoolean} = require('graphql')

var ContinentsInputType= new GraphQLInputObjectType({
    name:"ContinentsInputType",
    description:"Type for continents input",
    fields:()=>({   
        name: {
			type: GraphQLString
		},
		cardinal_direction: {
			type: GraphQLString
		},
		created_time: {
			type: GraphQLString
		},
		modified_time: {
			type: GraphQLString
		}
    })
})

module.exports=ContinentsInputType;