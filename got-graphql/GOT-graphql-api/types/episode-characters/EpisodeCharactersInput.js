var {GraphQLInputObjectType, GraphQLString,GraphQLBoolean} = require('graphql')

var EpisodeCharactersInputType= new GraphQLInputObjectType ({
    name:"EpisodeCharactersInputType",
    description:"Type for episode characters input",
    fields:()=>({
        id: {
			type: GraphQLString
        },
        episode:{
            type:GraphQLString
        },
        characters:{
            type:GraphQLString
        }
    })
})

module.exports=EpisodeCharactersInputType;