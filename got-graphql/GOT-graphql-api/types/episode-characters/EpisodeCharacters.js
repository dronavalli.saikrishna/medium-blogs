var {GraphQLObjectType, GraphQLString,GraphQLBoolean} = require('graphql')
var Db= require("../../db")

var EpisodeCharactersType= new GraphQLObjectType ({
    name:"EpisodeCharactersType",
    description:"Type for episode characters output",
    fields:()=>({
        id: {
			type: GraphQLString
        },
        episode:{
            type:GraphQLString
        },
		EpisodeDetails: {
            type: require("../episode/Episode"),
            resolve:(episode_characters,args)=>{
                return Db.models.episode.findOne({
                    where:{name:episode_characters.episode},
                    order:[
                        ['season','ASC'],
                        ['episode_number','ASC']
                    ]
                })
            }
        },
        characters:{
            type:GraphQLString
        },
		CharacterDetails: {
            type: require("../characters/Characters"),
            resolve:(episode_characters,args)=>{
                return Db.models.characters.findOne({
                    where:{name:episode_characters.characters}
                })
            }
		}
    })
})

module.exports=EpisodeCharactersType;