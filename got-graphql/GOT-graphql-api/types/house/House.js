var {GraphQLObjectType, GraphQLString,GraphQLBoolean,GraphQLList} = require('graphql')
var Db= require("../../db")

var House= new GraphQLObjectType ({
    name:"House",
    description:"Type for house output",
    fields:()=>({
        name: {
			type: GraphQLString
		},
		is_extinct: {
			type: GraphQLBoolean
		},
		coat_of_arms: {
			type: GraphQLString
		},
		words: {
			type: GraphQLString
		},
		overlord: {
			type: GraphQLString
        },
        OverLordDetails:{
            type:House,
            resolve:(house,args)=>{
                return Db.models.house.findOne({
                    where:{
                        name:house.obverlord
                    }
                })
            }
        },
		title: {
			type: GraphQLString
		},
		region: {
			type: GraphQLString
        },
        RegionDetails:{
            type:require("../region/Region"),
            resolve:(house,args)=>{
                return Db.models.region.findOne({
                    where:{
                        name:house.region
                    }
                })
            }
        },
		cadet_branch: {
			type: GraphQLString
		},
		founded: {
			type: GraphQLString
		},
		created_at: {
			type: GraphQLString
		},
		modified_at: {
			type: GraphQLString
		},
		image_link: {
			type: GraphQLString
		},
		CharactersInTheHouse:{
			type:new GraphQLList(require("../characters/Characters")),
			resolve:(house,args)=>{
				return Db.models.characters.findAll({
					where:{
						house:house.name
					}
				})
			}
		}
    })
})

module.exports=House;