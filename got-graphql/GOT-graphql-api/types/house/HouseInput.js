var {GraphQLInputObjectType, GraphQLString,GraphQLBoolean} = require('graphql')

var HouseInput= new GraphQLInputObjectType ({
    name:"HouseInput",
    description:"Type for house input",
    fields:()=>({
        name: {
			type: GraphQLString
		},
		is_extinct: {
			type: GraphQLBoolean
		},
		coat_of_arms: {
			type: GraphQLString
		},
		words: {
			type: GraphQLString
		},
		overlord: {
			type: GraphQLString
        },
		title: {
			type: GraphQLString
		},
		region: {
			type: GraphQLString
        },
		cadet_branch: {
			type: GraphQLString
		},
		founded: {
			type: GraphQLString
		},
		created_at: {
			type: GraphQLString
		},
		modified_at: {
			type: GraphQLString
		},
		image_link: {
			type: GraphQLString
		}
    })
})

module.exports=HouseInput;