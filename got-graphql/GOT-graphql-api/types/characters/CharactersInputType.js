var {GraphQLInputObjectType, GraphQLString,GraphQLBoolean} = require('graphql')

var CharactersInputType= new GraphQLInputObjectType({
    name:"CharactersInputType",
    description:"Type for Characters input type",
    fields:()=>({
        name: {
			type: GraphQLString
		},
		male: {
			type: GraphQLString
		},
		culture: {
			type: GraphQLString
		},
		age: {
			type: GraphQLString
        },
		actor: {
			type: GraphQLString
		},
		mother: {
			type: GraphQLString
        },
		father: {
			type: GraphQLString
        },
        heir:{
            type:GraphQLString
        },
        house:{
            type:GraphQLString
        },
		created_at: {
			type: GraphQLString
		},
		modified_at: {
			type: GraphQLString
		},
		spouse: {
			type: GraphQLString
        },
		allegiance: {
			type: GraphQLString
        },
		imageLink: {
			type: GraphQLString
		},
		slug: {
			type: GraphQLString
		}
    })
});

module.exports=CharactersInputType;