var {GraphQLObjectType, GraphQLString,GraphQLBoolean,GraphQLList} = require('graphql')
var Db= require("../../db")

var CharactersType= new GraphQLObjectType({
    name:"CharactersType",
    description:"Type for Characters output",
    fields:()=>({
        name: {
			type: GraphQLString
		},
		male: {
			type: GraphQLString
		},
		culture: {
			type: GraphQLString
        },
        CultureDetails:{
            type:require("../cultiure/Culture"),
            resolve:(characters,args)=>{
                return Db.models.culture.findOne({
                    where:{
                        culture_name:characters.culture
                    }
                })
            }
        },
		age: {
			type: GraphQLString
        },
        AgeDetails:{
            type:require("../age/Age"),
            resolve:(characters,args)=>{
                return Db.models.characters.findOne({
                    where:{name:characters.age}
                })
            }
        },
		actor: {
			type: GraphQLString
		},
		mother: {
			type: GraphQLString
        },
        MotherDetails:{
            type:CharactersType,
            resolve:(characters,args)=>{
                return Db.models.characters.findOne({
                    where:{name:characters.mother}
                })
            }
        },
		father: {
			type: GraphQLString
        },
        FatherDetails:{
            type:CharactersType,
            resolve:(characters,args)=>{
                return Db.models.characters.findOne({
                    where:{name:characters.father}
                })
            }
        },
        heir:{
            type:GraphQLString
        },
		HeirDetails: {
            type: CharactersType,
            resolve:(characters,args)=>{
                return Db.models.characters.findOne({
                    where:{name:characters.heir}
                })
            }
        },
        house:{
            type:GraphQLString
        },
		HouseDetails: {
            type: require("../house/House"),
            resolve:(characters,args)=>{
                return Db.models.house.findOne({
                    where:{name:characters.house}
                })
            }
		},
		created_at: {
			type: GraphQLString
		},
		modified_at: {
			type: GraphQLString
		},
		spouse: {
			type: GraphQLString
        },
        SpouseDetails: {
            type: CharactersType,
            resolve:(characters,args)=>{
                return Db.models.characters.findOne({
                    where:{name:characters.spouse}
                })
            }
		},
		allegiance: {
			type: GraphQLString
        },
        AllegianceDetails:{
            type:CharactersType,
            resolve:(characters,args)=>{
                return Db.models.characters.findOne({
                    where:{name:characters.allegiance}
                })
            }
        },
		imageLink: {
			type: GraphQLString
		},
		slug: {
			type: GraphQLString
        },
        EpisodesPartOf:{
            type:new GraphQLList(require("../episode-characters/EpisodeCharacters")),
            resolve:(characters,args)=>{
                return Db.models.episode_characters.findAll({
                    where:{
                        characters:characters.name
                    }
                })
            }
        }
    })
});

module.exports=CharactersType;