var { GraphQLInputObjectType, GraphQLString, GraphQLBoolean, GraphQLInt } = require('graphql')
var Db = require("../../db")

var EpisodeInputType = new GraphQLInputObjectType({
    name: "EpisodeInputType",
    description: "Type for episode input",
    fields: () => ({
        name: {
            type: GraphQLString
        },
        season: {
            type: GraphQLInt
        },
        episode_number: {
            type: GraphQLInt
        },
        previous_episode: {
            type: GraphQLString
        },
        next_episode: {
            type: GraphQLString
        },
        director: {
            type: GraphQLString
        },
        created_at: {
            type: GraphQLString
        },
        modified_at: {
            type: GraphQLString
        }
    })
})

module.exports = EpisodeInputType;