var {GraphQLObjectType, GraphQLString,GraphQLBoolean,GraphQLInt,GraphQLList} = require('graphql')
var Db= require("../../db")

var EpisodeType= new GraphQLObjectType({
    name:"EpisodeType",
    description:"Type for episode output",
    fields:()=>({
        name: {
			type: GraphQLString
		},
		season: {
			type: GraphQLInt
		},
		episode_number: {
			type: GraphQLInt
		},
		previous_episode: {
			type: GraphQLString
        },
        PreviousEpisodeDetails:{
            type:EpisodeType,
            resolve:(episode,args)=>{
                return Db.models.episode.findOne({
                    where:{name:episode.previous_episode}
                })
            }
        },
		next_episode: {
			type: GraphQLString
        },
        NextEpisodeDetails:{
            type:EpisodeType,
            resolve:(episode,args)=>{
                return Db.models.episode.findOne({
                    where:{name:episode.next_episode}
                })
            }
        },
		director: {
			type: GraphQLString
		},
		created_at: {
			type: GraphQLString
		},
		modified_at: {
			type: GraphQLString
        },
        EpisodeCharacters:{
            type:new GraphQLList(require("../episode-characters/EpisodeCharacters")),
            resolve:(episode,args)=>{
                return Db.models.episode_characters.findAll({
                    where:{
                        episode:episode.name,
                    }
                })
            }
        }
    })
})

module.exports=EpisodeType;