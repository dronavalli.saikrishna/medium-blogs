var {GraphQLInputObjectType, GraphQLString,GraphQLBoolean} = require('graphql')
var Db= require("../../db")

var EventsInput= new GraphQLInputObjectType ({
    name:"EventsInput",
    description:"Type for events input",
    fields:()=>({
        name: {
			type: GraphQLString
		},
		date: {
			type: GraphQLString
		},
		age: {
			type: GraphQLString
		},
		created_at: {
			type: GraphQLString
		},
		modified_time: {
			type: GraphQLString
		}
    })
})


module.exports=EventsInput;