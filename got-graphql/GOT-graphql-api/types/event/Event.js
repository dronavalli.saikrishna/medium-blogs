var {GraphQLObjectType, GraphQLString,GraphQLBoolean} = require('graphql')
var Db= require("../../db")

var Events= new GraphQLObjectType ({
    name:"Events",
    description:"Type for events output",
    fields:()=>({
        name: {
			type: GraphQLString
		},
		date: {
			type: GraphQLString
		},
		age: {
			type: GraphQLString
        },
        AgeDetails:{
            type:require("../age/Age"),
            resolve:(event,args)=>{
                return Db.models.age.findOne({
                    where:{
                        name:event.age
                    }
                })
            }
        },
		created_at: {
			type: GraphQLString
		},
		modified_time: {
			type: GraphQLString
		}
    })
})


module.exports=Events;