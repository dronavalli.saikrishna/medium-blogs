var {GraphQLInputObjectType, GraphQLString,GraphQLBoolean} = require('graphql')

var CultureInputType= new GraphQLInputObjectType({
    name:"CultureInputType",
    description:"Type for culture input",
    fields:()=>({
        culture_name: {
			type:GraphQLString
		},
		created_time: {
			type: GraphQLString
		},
		modified_time: {
			type: GraphQLString
		}
    })
})

module.exports=CultureInputType;