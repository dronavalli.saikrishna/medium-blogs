var {GraphQLObjectType, GraphQLString,GraphQLBoolean,GraphQLList} = require('graphql')
var Db= require("../../db")

var CultureType= new GraphQLObjectType ({
    name:"CultureType",
    description:"Type for culture output",
    fields:()=>({
        culture_name: {
			type:GraphQLString
		},
		created_time: {
			type: GraphQLString
		},
		modified_time: {
			type: GraphQLString
		},
		CultureFollowdBy:{
			type: new GraphQLList(require("../characters/Characters")),
			resolve:(culture,args)=>{
				return Db.models.characters.findAll({
					where:{
						culture:culture.culture_name
					}
				})
			}
		}
    })
})

module.exports=CultureType;