var {GraphQLObjectType, GraphQLString,GraphQLBoolean} = require('graphql')

var CityType= new GraphQLObjectType({
    name:"CityType",
    description:"Type for City output",
    fields:()=>({
        name: {
			type: GraphQLString
		},
		x_coordinate: {
			type: GraphQLString
		},
		y_coordinate: {
			type:GraphQLString
		},
		type: {
			type: GraphQLString
		},
		priority: {
			type:GraphQLString
		},
		link: {
			type: GraphQLString
		}
    })
})
module.exports=CityType;