var {GraphQLInputObjectType, GraphQLString,GraphQLBoolean} = require('graphql')

var CityInputType= new GraphQLInputObjectType({
    name:"CityInputType",
    description:"Type for City input type",
    fields:()=>({
        name: {
			type: GraphQLString
		},
		x_coordinate: {
			type: GraphQLString
		},
		y_coordinate: {
			type:GraphQLString
		},
		type: {
			type: GraphQLString
		},
		priority: {
			type:GraphQLString
		},
		link: {
			type: GraphQLString
		}
    })
})
module.exports=CityInputType;