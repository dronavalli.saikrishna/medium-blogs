var { GraphQLObjectType, GraphQLString, GraphQLBoolean, GraphQLList } = require('graphql')
var Db = require("../../db")

var AgeOutputType = new GraphQLObjectType({
    name: "Age",
    description: "Type for Ageoutput",
    fields: () => ({
        name: {
            type: GraphQLString
        },
        predecessor: {
            type: GraphQLString
        },
        PredecessorDetails: {
            type: AgeOutputType,
            resolve: (age, args) => {
                return Db.models.age.findOne({
                    where: { name: age.predecessor }
                })
            }
        },
        successor: {
            type: GraphQLString
        },
        SuccessorDetails: {
            type: AgeOutputType,
            resolve: (age, args) => {
                return Db.models.age.findOne({
                    where: { name: age.successor }
                })
            }
        },
        CharactersInTheAge: {
            type: new GraphQLList(require("../characters/Characters")),
            resolve: (age, args) => {
                return Db.models.characters.findAll({
                    where: {
                        age: age.name
                    }
                })
            }
        },
        EventsHappened: {
            type: new GraphQLList(require("../event/Event")),
            resolve: (age, args) => {
                return Db.models.event.findAll({
                    where: {
                        age: age.name
                    }
                })
            }
        }
    })
});

module.exports = AgeOutputType;

