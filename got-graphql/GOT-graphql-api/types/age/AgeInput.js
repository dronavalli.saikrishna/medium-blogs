var { GraphQLInputObjectType, GraphQLString, GraphQLBoolean } = require('graphql')

var AgeInputType = new GraphQLInputObjectType({
    name: "AgeInput",
    description: "Type for AgeInput",
    fields: () => ({
        name: {
            type: GraphQLString
        },
        predecessor: {
            type: GraphQLString
        },
        successor: {
            type: GraphQLString
        }
    })
});

module.exports = AgeInputType;

