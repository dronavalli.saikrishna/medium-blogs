To Run the project follow these instructions.
1. CLone the repository. Make sure you have `npm` and `node.js` installed on your system.
2. Go to sequelize-auto-modal-creator using `cd sequelize-auto-modal-creator`
3. Run `npm install` to get all the dependencies from `package.json`.
4. Make sure you edit and give your Db details in `index.js`
5. run `node index.js`.
6. This will create model classes from your schema in a folder called `models`
7. To know more about importing these schemas in your nodejs application [read my blog here](https://medium.com/@saikrishna.d/get-create-database-models-for-existing-database-tables-with-nodejs-python-51dc0856abd7).